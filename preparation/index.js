const Mysql = require('../mysql');

class Preparation {
  //constructor(redis, postgre) {
  constructor(redis) {
    this.redis = redis;
    // this.postgre = postgre;

    this.container = '1';
    this.marketFulls = {};
    this.marketNames = {};

    this.delistMarkets = ['BNBBTC', 'CLOAKBTC', 'MODBTC', 'SALTBTC', 'SUBBTC', 'WINGSBTC', 'NULSBTC', 'ENJBTC', 'EVXBTC', 'VIBBTC', 'WTCBTC', 'LINKBTC'];

    /*
    this.group1 = ['ETHBTC', 'XRPBTC', 'LTCBTC', 'BCHABCBTC', 'EOSBTC', 'ALGOBTC', 'TRXBTC', 'XLMBTC', 'ADABTC', 'XMRBTC', 'DASHBTC', 'LINKBTC'];
    this.group2 = ['IOTABTC', 'NEOBTC', 'ATOMBTC', 'ETCBTC', 'XEMBTC', 'ONTBTC', 'BTGBTC', 'ZEXBTC', 'DOGEBTC', 'VETBTC', 'BATBTC', 'QTUMBTC', 'DCRBTC', 'HOTBTC', 'RVNBTC', 'OMGBTC', 'KMDBTC', 'BCDBTC', 'HCBTC', 'NPXSBTC', 'LSKBTC'];
    this.group3 = ['WAVESBTC', 'NANOBTC', 'REPBTC', 'ZRXBTC', 'SCBTC', 'BTSBTC', 'ICXBTC', 'ZILBTC', 'XVGBTC', 'IOSTBTC', 'THETABTC', 'GXSBTC', 'AEBTC', 'STEEMBTC', 'WTCBTC', 'MCOBTC', 'ENJBTC', 'SNTBTC', 'DENTBTC', 'ELFBTC', 'ARDRBTC', 'XZCBTC', 'RENBTC', 'GNTBTC', 'STRATBTC', 'ZENBTC', 'NASBTC', 'NULSBTC', 'MANABTC', 'MATICBTC'];
    this.group4 = ['DGDBTC', 'FTMBTC', 'ERDBTC', 'ENGBTC', 'LOOMBTC', 'RTUELBTC', 'ARKBTC', 'PPTBTC', 'CMTBTC', 'LRCBTC', 'BNTBTC', 'KNCBTC', 'IOTXBTC', 'POWRBTC', 'QKCBTC', 'AIONBTC', 'WANBTC', 'FETBTC', 'BRDBTC', 'PIVXBTC', 'ONEBTC', 'STORJBTC', 'POLYBTC', 'RLCBTC', 'GASBTC', 'CELRBTC', 'FUNBTC', 'EDOBTC', 'TNTBTC', 'SKYBTC', 'GRCBTC', 'CVCBTC', 'MTLBTC', 'MITHBTC', 'SYSBTC', 'CNDBTC', 'NXSBTC', 'STORMBTC', 'MFTBTC'];
    this.group5 = ['ONGBTC', 'CDTBTC', 'AGIBTC', 'POEBTC', 'NEBLBTC', 'MDABTC', 'BQXBTC', 'TNBBTC', 'GTOBTC', 'DATABTC', 'NCASHBTC', 'REQBTC', 'EVXBTC', 'QSPBTC', 'INSBTC', 'OSTBTC', 'RCNBTC', 'NAVBTC', 'GOBTC', 'WABIBTC', 'ADXBTC', 'GVTBTC', 'VIABTC', 'OAXBTC', 'BLZBTC', 'ASTBTC', 'DNTBTC', 'PHBBTC', 'VIBBTC', 'YOYOBTC', 'KEYBTC', 'VIBEBTC', 'LENDBTC', 'QLCBTC', 'MTHBTC', 'POABTC', 'DLTBTC', 'BCPTBTC', 'SNMBTC', 'APPCBTC', 'ARNBTC', 'DOCKBTC', 'WPRBTC', 'AMBBTC', 'FUELBTC', 'LUNBTC', 'BTCBBTC', 'RDNBTC'];
    */

    this.group1 = ['MDABTC', 'ETHBTC', 'XRPBTC', 'LTCBTC', 'BCHABCBTC', 'EOSBTC', 'ALGOBTC', 'TRXBTC', 'XLMBTC', 'ADABTC', 'XMRBTC', 'DASHBTC', 'LINKBTC'];
    this.group2 = ['IOTABTC', 'NEOBTC', 'ATOMBTC', 'ETCBTC', 'XEMBTC', 'ONTBTC', 'BTGBTC', 'ZEXBTC', 'DOGEBTC', 'VETBTC', 'BATBTC', 'QTUMBTC', 'DCRBTC', 'HOTBTC', 'RVNBTC', 'OMGBTC', 'KMDBTC', 'BCDBTC', 'HCBTC', 'NPXSBTC', 'LSKBTC'];
    this.group3 = ['WAVESBTC', 'NANOBTC', 'REPBTC', 'ZRXBTC', 'SCBTC', 'BTSBTC', 'ICXBTC', 'ZILBTC', 'XVGBTC', 'IOSTBTC', 'THETABTC', 'GXSBTC', 'AEBTC', 'STEEMBTC', 'WTCBTC', 'MCOBTC', 'ENJBTC', 'SNTBTC', 'DENTBTC', 'ELFBTC', 'ARDRBTC', 'XZCBTC', 'RENBTC', 'GNTBTC', 'STRATBTC', 'ZENBTC', 'NASBTC', 'NULSBTC', 'MANABTC', 'MATICBTC'];
    this.group4 = ['DGDBTC', 'FTMBTC', 'ERDBTC', 'ENGBTC', 'LOOMBTC', 'RTUELBTC', 'ARKBTC', 'PPTBTC', 'CMTBTC', 'LRCBTC', 'BNTBTC', 'KNCBTC', 'IOTXBTC', 'POWRBTC', 'QKCBTC', 'AIONBTC', 'WANBTC', 'FETBTC', 'BRDBTC', 'PIVXBTC', 'ONEBTC', 'STORJBTC', 'POLYBTC', 'RLCBTC', 'GASBTC', 'CELRBTC', 'FUNBTC', 'EDOBTC', 'TNTBTC', 'SKYBTC', 'GRCBTC', 'CVCBTC', 'MTLBTC', 'MITHBTC', 'SYSBTC', 'CNDBTC', 'NXSBTC', 'STORMBTC', 'MFTBTC'];
    this.group5 = ['ONGBTC', 'CDTBTC', 'AGIBTC', 'POEBTC', 'NEBLBTC',  'BQXBTC', 'TNBBTC', 'GTOBTC', 'DATABTC', 'NCASHBTC', 'REQBTC', 'EVXBTC', 'QSPBTC', 'INSBTC', 'OSTBTC', 'RCNBTC', 'NAVBTC', 'GOBTC', 'WABIBTC', 'ADXBTC', 'GVTBTC', 'VIABTC', 'OAXBTC', 'BLZBTC', 'ASTBTC', 'DNTBTC', 'PHBBTC', 'VIBBTC', 'YOYOBTC', 'KEYBTC', 'VIBEBTC', 'LENDBTC', 'QLCBTC', 'MTHBTC', 'POABTC', 'DLTBTC', 'BCPTBTC', 'SNMBTC', 'APPCBTC', 'ARNBTC', 'DOCKBTC', 'WPRBTC', 'AMBBTC', 'FUELBTC', 'LUNBTC', 'BTCBBTC', 'RDNBTC'];

    return this;
  }

  async getMarketsForWork() {
    try {
      // const marketsObj = await this.getMarketsFromPostgreDB();
      const marketsObj = await this.getMarketsFromMysqlDB();
      if (marketsObj === null) return null;

      this.marketFulls = marketsObj.f;
      this.marketNames = marketsObj.n;

      const resultStr = await this.keepDatasForWork();
      return {
        step1: marketsObj,
        step2: resultStr,
      };
    } catch (err) {
      return err;
    }
  }

  /*
  getMarketsFromPostgreDB() {
    return new Promise((resolve, reject) => {
      let sql = '';

      if (process.env.INST === 'INST1') {
        sql = `SELECT * FROM public.monitoringsbinance_mar_total_sim WHERE ( container = $1 AND market IN ('ETHBTC', 'XRPBTC', 'LTCBTC', 'BCHABCBTC', 'EOSBTC', 'ALGOBTC', 'TRXBTC', 'XLMBTC', 'ADABTC', 'XMRBTC', 'DASHBTC', 'LINKBTC') );`;
      }

      if (process.env.INST === 'INST2') {
        sql = `SELECT * FROM public.monitoringsbinance_mar_total_sim WHERE ( container = $1 AND market IN ('IOTABTC', 'NEOBTC', 'ATOMBTC', 'ETCBTC', 'XEMBTC', 'ONTBTC', 'BTGBTC', 'ZEXBTC', 'DOGEBTC', 'VETBTC', 'BATBTC', 'QTUMBTC', 'DCRBTC', 'HOTBTC', 'RVNBTC', 'OMGBTC', 'KMDBTC', 'BCDBTC', 'HCBTC', 'NPXSBTC', 'LSKBTC') );`;
      }

      if (process.env.INST === 'INST3') {
        sql = `SELECT * FROM public.monitoringsbinance_mar_total_sim WHERE ( container = $1 AND market IN ('WAVESBTC', 'NANOBTC', 'REPBTC', 'ZRXBTC', 'SCBTC', 'BTSBTC', 'ICXBTC', 'ZILBTC', 'XVGBTC', 'IOSTBTC', 'THETABTC', 'GXSBTC', 'AEBTC', 'STEEMBTC', 'WTCBTC', 'MCOBTC', 'ENJBTC', 'SNTBTC', 'DENTBTC', 'ELFBTC', 'ARDRBTC', 'XZCBTC', 'RENBTC', 'GNTBTC', 'STRATBTC', 'ZENBTC', 'NASBTC', 'NULSBTC', 'MANABTC', 'MATICBTC') );`;
      }

      if (process.env.INST === 'INST4') {
        sql = `SELECT * FROM public.monitoringsbinance_mar_total_sim WHERE ( container = $1 AND market IN ('DGDBTC', 'FTMBTC', 'ERDBTC', 'ENGBTC', 'LOOMBTC', 'RTUELBTC', 'ARKBTC', 'PPTBTC', 'CMTBTC', 'LRCBTC', 'BNTBTC', 'KNCBTC', 'IOTXBTC', 'POWRBTC', 'QKCBTC', 'AIONBTC', 'WANBTC', 'FETBTC', 'BRDBTC', 'PIVXBTC', 'ONEBTC', 'STORJBTC', 'POLYBTC', 'RLCBTC', 'GASBTC', 'CELRBTC', 'FUNBTC', 'EDOBTC', 'TNTBTC', 'SKYBTC', 'GRCBTC', 'CVCBTC', 'MTLBTC', 'MITHBTC', 'SYSBTC', 'CNDBTC', 'NXSBTC', 'STORMBTC', 'MFTBTC') );`;
      }

      if (process.env.INST === 'INST5') {
        sql = `SELECT * FROM public.monitoringsbinance_mar_total_sim WHERE ( container = $1 AND market IN ('ONGBTC', 'CDTBTC', 'AGIBTC', 'POEBTC', 'NEBLBTC', 'MDABTC', 'BQXBTC', 'TNBBTC', 'GTOBTC', 'DATABTC', 'NCASHBTC', 'REQBTC', 'EVXBTC', 'QSPBTC', 'INSBTC', 'OSTBTC', 'RCNBTC', 'NAVBTC', 'GOBTC', 'WABIBTC', 'ADXBTC', 'GVTBTC', 'VIABTC', 'OAXBTC', 'BLZBTC', 'ASTBTC', 'DNTBTC', 'PHBBTC', 'VIBBTC', 'YOYOBTC', 'KEYBTC', 'VIBEBTC', 'LENDBTC', 'QLCBTC', 'MTHBTC', 'POABTC', 'DLTBTC', 'BCPTBTC', 'SNMBTC', 'APPCBTC', 'ARNBTC', 'DOCKBTC', 'WPRBTC', 'AMBBTC', 'FUELBTC', 'LUNBTC', 'BTCBBTC', 'RDNBTC') );`;
      }

      if (process.env.ROBOT_MODE === 'dev') {
        // sql = `SELECT * FROM public.monitoringsbinance_mar_total_sim WHERE ( container = $1 AND market NOT IN ('BTCUSDT', 'TUSDBTC', 'BCNBTC', 'TRIGBTC', 'ICNBTC', 'CHATBTC', 'BCCBTC') ) ${process.env.POSTGRE_RANGE_COINS};`;
      }

      if (process.env.ROBOT_MODE === 'prod') {
        // sql = `SELECT * FROM public.monitoringsbinance WHERE ( container = $1 AND market IN ${process.env.POSTGRE_RANGE_COINS};`;

        // sql = `SELECT * FROM public.monitoringsbinance WHERE ( container = $1 AND market NOT IN ('BTCUSDT', 'TUSDBTC', 'BCNBTC', 'TRIGBTC', 'ICNBTC', 'CHATBTC', 'BCCBTC') ) ${process.env.POSTGRE_RANGE_COINS};`;
      }

      if (sql !== '') {
        this.postgre.query(sql, [this.container], (err, result) => {
          if (err) reject(err);

          let counter = 0;
          const marketFulls = [];
          const marketNames = [];
          if (result.rowCount >= 1) {
            result.rows.forEach((dataset) => {
              if (!this.delistMarkets.includes(dataset.market)) {
                marketFulls.push(dataset);
                marketNames.push(dataset.market);
              }
              counter += 1;
              if (result.rowCount === counter) {
                counter = 0;
                const obj = {
                  f: marketFulls,
                  n: marketNames,
                };
                resolve(obj);
              }
            });
          } else {
            resolve(null);
          }
        });
      } else {
        resolve(null);
      }
    });
  }
  */

  getMarketsFromMysqlDB() {
    return new Promise((resolve, reject) => {

      const mysql = new Mysql();

      let sql = '';

      if (process.env.INST === 'INST1') {
        sql = `SELECT * FROM monitoringsbinance_mar_total_sim WHERE ( container = ? AND market IN ('MDABTC', 'ETHBTC', 'XRPBTC', 'LTCBTC', 'BCHABCBTC', 'EOSBTC', 'ALGOBTC', 'TRXBTC', 'XLMBTC', 'ADABTC', 'XMRBTC', 'DASHBTC', 'LINKBTC') );`;
      }

      if (process.env.INST === 'INST2') {
        sql = `SELECT * FROM monitoringsbinance_mar_total_sim WHERE ( container = ? AND market IN ('IOTABTC', 'NEOBTC', 'ATOMBTC', 'ETCBTC', 'XEMBTC', 'ONTBTC', 'BTGBTC', 'ZEXBTC', 'DOGEBTC', 'VETBTC', 'BATBTC', 'QTUMBTC', 'DCRBTC', 'HOTBTC', 'RVNBTC', 'OMGBTC', 'KMDBTC', 'BCDBTC', 'HCBTC', 'NPXSBTC', 'LSKBTC') );`;
      }

      if (process.env.INST === 'INST3') {
        sql = `SELECT * FROM monitoringsbinance_mar_total_sim WHERE ( container = ? AND market IN ('WAVESBTC', 'NANOBTC', 'REPBTC', 'ZRXBTC', 'SCBTC', 'BTSBTC', 'ICXBTC', 'ZILBTC', 'XVGBTC', 'IOSTBTC', 'THETABTC', 'GXSBTC', 'AEBTC', 'STEEMBTC', 'WTCBTC', 'MCOBTC', 'ENJBTC', 'SNTBTC', 'DENTBTC', 'ELFBTC', 'ARDRBTC', 'XZCBTC', 'RENBTC', 'GNTBTC', 'STRATBTC', 'ZENBTC', 'NASBTC', 'NULSBTC', 'MANABTC', 'MATICBTC') );`;
      }

      if (process.env.INST === 'INST4') {
        sql = `SELECT * FROM monitoringsbinance_mar_total_sim WHERE ( container = ? AND market IN ('DGDBTC', 'FTMBTC', 'ERDBTC', 'ENGBTC', 'LOOMBTC', 'RTUELBTC', 'ARKBTC', 'PPTBTC', 'CMTBTC', 'LRCBTC', 'BNTBTC', 'KNCBTC', 'IOTXBTC', 'POWRBTC', 'QKCBTC', 'AIONBTC', 'WANBTC', 'FETBTC', 'BRDBTC', 'PIVXBTC', 'ONEBTC', 'STORJBTC', 'POLYBTC', 'RLCBTC', 'GASBTC', 'CELRBTC', 'FUNBTC', 'EDOBTC', 'TNTBTC', 'SKYBTC', 'GRCBTC', 'CVCBTC', 'MTLBTC', 'MITHBTC', 'SYSBTC', 'CNDBTC', 'NXSBTC', 'STORMBTC', 'MFTBTC') );`;
      }

      if (process.env.INST === 'INST5') {
        sql = `SELECT * FROM monitoringsbinance_mar_total_sim WHERE ( container = ? AND market IN ('ONGBTC', 'CDTBTC', 'AGIBTC', 'POEBTC', 'NEBLBTC', 'BQXBTC', 'TNBBTC', 'GTOBTC', 'DATABTC', 'NCASHBTC', 'REQBTC', 'EVXBTC', 'QSPBTC', 'INSBTC', 'OSTBTC', 'RCNBTC', 'NAVBTC', 'GOBTC', 'WABIBTC', 'ADXBTC', 'GVTBTC', 'VIABTC', 'OAXBTC', 'BLZBTC', 'ASTBTC', 'DNTBTC', 'PHBBTC', 'VIBBTC', 'YOYOBTC', 'KEYBTC', 'VIBEBTC', 'LENDBTC', 'QLCBTC', 'MTHBTC', 'POABTC', 'DLTBTC', 'BCPTBTC', 'SNMBTC', 'APPCBTC', 'ARNBTC', 'DOCKBTC', 'WPRBTC', 'AMBBTC', 'FUELBTC', 'LUNBTC', 'BTCBBTC', 'RDNBTC') );`;
      }

      if (sql !== '') {
        mysql.connection.query({
            sql: sql,
            timeout: 40000,
            values: [this.container],
        }, (err, result) => {
          if (err) {
            console.log(err);
            reject(err);
          }

          let counter = 0;
          const marketFulls = [];
          const marketNames = [];

          if (result.length > 0) {
            result.forEach((dataset, index) => {
              if (!this.delistMarkets.includes(dataset.market)) {
                marketFulls.push(dataset);
                marketNames.push(dataset.market);
              }
              counter += 1;
              if (index === result.length - 1) {
                counter = 0;
                const obj = {
                  f: marketFulls,
                  n: marketNames,
                };
                // mysql.doneMysql();
                mysql.connection.end((err) => {
                  resolve(obj);
                });
              }
            });
          } else {
            // mysql.doneMysql();
            mysql.connection.end((err) => {
              resolve(null);
            });
          }
        });
      } else {
        // mysql.doneMysql();
        mysql.connection.end((err) => {
          resolve(null);
        });
      }
    });
  }

  keepDatasForWork() {
    return new Promise((resolve, reject) => {
      let counterYes = 0;
      const counterNo = 0;
      const marketslen = this.marketFulls.length;

      this.marketFulls.forEach((dataset) => {
        if (dataset.typetrd === 'buy' || dataset.typetrd === 'sell') {
          let additionalDataForMarket = 0;
          let n = dataset.additionaldata;
          n = parseFloat(n);
          n = (typeof n === 'string') ? n : n.toString();
          if (n.indexOf('e') !== -1) {
            additionalDataForMarket = parseInt(n.split('e')[1], 0) * -1;
          } else {
            const separator = (1.1).toString().split('1')[1];
            const parts = n.split(separator);
            const t = parts.length > 1 ? parts[parts.length - 1].length : 0;
            additionalDataForMarket = t;
          }

          this.redis.pipeline([
            ['set', `${dataset.market}:buysellnow`, 0.0],
            ['set', `${dataset.market}:amount`, dataset.amount],
            ['set', `${dataset.market}:deep`, dataset.deep],
            ['set', `${dataset.market}:typetrd`, dataset.typetrd],
            ['set', `${dataset.market}:thiswallpricesell`, 0.0],
            ['set', `${dataset.market}:thiswallpricebuy`, 0.0],
            ['set', `${dataset.market}:thiswallvolumebuy`, 0.0],
            ['set', `${dataset.market}:lastwallprice`, 0.0],
            ['set', `${dataset.market}:ratebuy`, dataset.ratebuy],
            ['set', `${dataset.market}:spread`, dataset.note],
            ['set', `${dataset.market}:additionaldata`, dataset.additionaldata],
            ['set', `${dataset.market}:stoploss`, dataset.stoploss],
            ['set', `${dataset.market}:raz`, additionalDataForMarket],
          ]).exec((err) => {
            if (err) reject(err);

            counterYes += 1;
            if (marketslen === (counterYes + counterNo)) {
              resolve('All markets keeping in redis.');
            }
          });
        } else counterYes += 1;
      });
    });
  }
}

module.exports = Preparation;