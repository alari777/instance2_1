// const Algorithmsbuy = require('./algorithms/buy');
// const algobuy   = new Algorithmsbuy();

// const Actionsbuy = require('./actions/buy');
// const actionbuy  = new Actionsbuy();

// const Algorithmssell = require('./algorithms/sell');
// const algosell = new Algorithmssell();

// const Actionssell = require('./actions/sell');
// const actionsell  = new Actionssell();

class Market {
  constructor(connections, binancecalcs, symbol) {
    this.redis = connections.redis;
    this.market = symbol;

    // this.algobuy   = new Algorithmsbuy(this);
    // this.actionbuy = new Actionsbuy(connections, binancecalcs, marketHistory, symbol);
    // this.algosell   = algosell;
    // this.acyionsell = actionsell;

    return this;
  }

  async router() {
    return new Promise((resolve, reject) => {
      this.redis.pipeline([
        ['get', `${this.market}:typetrd`],
      ]).exec((err, results) => {
        if (err) reject(err);

        resolve(results[0][1]);
      });
    });
  }
}

module.exports = Market;