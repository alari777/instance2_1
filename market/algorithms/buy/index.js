const settingsBuy = require('./setting-buy.json');

class Algobuy {
  constructor(connection, binancecalcs, binancekeeping, symbol) {
    this.redis = connection.redis;
    this.buysettings = settingsBuy;
    this.market = symbol;

    this.fixPriceBuy = 0;
    this.fixVolumeBuy = 0;
    this.raz = 0;
    this.stepsize = 0;

    this.binancecalcs = binancecalcs;
    this.datasBids = binancecalcs.datasBids;
    this.datasAsks = binancecalcs.datasAsks;

    this.binancekeeping = binancekeeping;
    this.marketHistory = new Map();

    return this;
  }

  async main() {
    await this.setBuySellNow();
    const getBuyDatas = await this.getBuyDatas();
    const depthAsks20 = await this.binancecalcs.calcWall(parseFloat(process.env.INST_BUY_1)); // 40
    // if (this.market === 'IOTABTC') console.log(depthAsks20);
    let depthAsks6090 = null;
    if (process.env.ROBOT_MODE === 'dev') {
      depthAsks6090 = depthAsks20; // await this.binancecalcs.calcWall(60);
    }

    if (process.env.ROBOT_MODE === 'prod') {
      depthAsks6090 = depthAsks20; // await this.binancecalcs.calcWall(60);
    }

    if (getBuyDatas === null) {
      const part11 = parseFloat(this.datasAsks[0].Rate) - parseFloat(this.datasBids[0].Rate);
      const part22 = part11 / parseFloat(this.datasBids[0].Rate) * 100;
      const spreadBuy = part22.toFixed(2);

      // if (spreadBuy < 0.3 && depthAsks6090 !== null) {
        // if (this.market === 'ETHBTC') console.log(this.market, this.FixPriceBuy, this.FixVolumeBuy);
        if (this.FixPriceBuy === 0.0) {
          this.FixPriceBuy = depthAsks6090.Rate;
          this.FixVolumeBuy = depthAsks6090.Quantity;

          await this.setFixDatas();
        } else if (this.FixPriceBuy > depthAsks6090.Rate) {
          this.FixPriceBuy = depthAsks6090.Rate;
          this.FixVolumeBuy = depthAsks6090.Quantity;

          await this.setFixDatas();
          // console.log(this.market, this.FixPriceBuy, this.FixVolumeBuy, setFixDatas);
        } else {
          if (this.FixPriceBuy === parseFloat(this.datasAsks[0].Rate)) {
            return ('nothing do');
          }

          // if (fix_buy < ask[0])
          if (this.FixPriceBuy < parseFloat(this.datasAsks[0].Rate)) {
            this.marketHistory = await this.binancekeeping.recentTrades(this.market);
            // console.log(this.market, this.FixPriceBuy, "#2", this.marketHistory);
            if (
              // this.FixVolumeBuy * this.buysettings.c02 > depthAsks6090.Quantity
              // this.FixVolumeBuy * this.buysettings.c04 > depthAsks6090.Quantity
              this.FixVolumeBuy * this.buysettings.c02 > depthAsks20.Quantity
                && this.marketHistory.has(this.FixPriceBuy)
            ) {
              const getMH = parseFloat(this.marketHistory.get(this.FixPriceBuy));
              if (getMH > this.FixVolumeBuy * this.buysettings.marketH08) {
                console.log(this.market, this.marketHistory);
                return ('buy now');
              }

              this.FixPriceBuy = 0.0;
              this.FixVolumeBuy = 0.0;

              await this.setFixDatas();
            } else {
              this.FixPriceBuy = 0.0;
              this.FixVolumeBuy = 0.0;

              await this.setFixDatas();
            }
          }

          if (this.FixPriceBuy > parseFloat(this.datasAsks[0].Rate)) {
            return ('nothing do');
          }
        }
      // } else {
        // console.log(this.market, spreadBuy);
        // console.log(depthAsks6090);
        // return ('nothing do');
      // }
    } else return ('nothing do');

    return ('nothing do');
  }

  getBuyDatas() {
    return new Promise((resolve, reject) => {
      this.redis.pipeline([
        ['get', `${this.market}:thiswallpricebuy`],
        ['get', `${this.market}:thiswallvolumebuy`],
        ['get', `${this.market}:raz`],
        ['get', `${this.market}:additionaldata`],
        ['get', `${this.market}:amount`],
        ['get', `${this.market}:deep`],
      ]).exec((err, results) => {
        if (err) reject(err);

        this.FixPriceBuy = parseFloat(results[0][1]);
        this.FixVolumeBuy = parseFloat(results[1][1]);
        this.Raz = parseFloat(results[2][1]);
        this.stepsize = results[3][1];
        this.amount = parseFloat(results[4][1]);
        this.deep = parseFloat(results[5][1]);

        resolve(null);
      });
    });
  }

  setFixDatas() {
    return new Promise((resolve, reject) => {
      this.redis.pipeline([
        ['set', `${this.market}:thiswallpricebuy`, this.FixPriceBuy],
        ['set', `${this.market}:thiswallvolumebuy`, this.FixVolumeBuy],
      ]).exec((err) => {
        if (err) reject(err);

        resolve(null);
      });
    });
  }

  setBuySellNow() {
    return new Promise((resolve, reject) => {
      this.redis.pipeline([
        ['set', `${this.market}:buysellnow`, this.datasAsks[0].Rate],
      ]).exec((err) => {
        if (err) reject(err);

        resolve(null);
      });
    });
  }

  get FixPriceBuy() {
    return parseFloat(this.fixPriceBuy);
  }

  set FixPriceBuy(value) {
    this.fixPriceBuy = parseFloat(value);
  }

  get FixVolumeBuy() {
    return parseFloat(this.fixVolumeBuy);
  }

  set FixVolumeBuy(value) {
    this.fixVolumeBuy = parseFloat(value);
  }

  get Raz() {
    return parseFloat(this.raz);
  }

  set Raz(value) {
    this.raz = parseFloat(value);
  }
}

module.exports = Algobuy;