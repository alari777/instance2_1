const settingsSell = require('./setting-sell.json');

class Algosell {
  constructor(connection, binancecalcs, symbol) {
    this.redis = connection.redis;
    this.redissub = connection.redissub;
    this.redispub = connection.redispub;

    this.sellsettings = settingsSell;
    this.market = symbol;

    this.fixPriceSell = 0;
    this.lastPriceSell = 0;
    this.amount = 0;
    this.rateBuyBuy = 0;
    this.stoploss = 0;
    this.raz = 0;
    this.stepsize = 0;

    this.binancecalcs = binancecalcs;
    this.datasBids = binancecalcs.datasBids;
    this.datasAsks = binancecalcs.datasAsks;

    return this;
  }

  async main() {
    await this.setBuySellNow();
    const getBuyDatas = await this.getSellDatas();
    const depthBuy90 = await this.binancecalcs.calcWall(40);
    const depthBuy90Second = await this.binancecalcs.calcWallSecond(40);
    const depthAsks40Plus1 = await this.binancecalcs.calcForWall(40, 1);
    const depthAsks40Plus21 = await this.binancecalcs.calcForWall(40, parseFloat(process.env.INST_SELL_1)); // 21

    let bidsPosition = 0;
    if (process.env.ROBOT_MODE === 'dev') {
      bidsPosition = 11;
    }

    if (process.env.ROBOT_MODE === 'prod') {
      bidsPosition = 11;
    }

    if (getBuyDatas === null && bidsPosition !== 0) {
      /*
      if (parseFloat(this.datasAsks[4].Rate) >= depthBuy90.Rate
          && depthBuy90.Quantity * 0.1 > depthBuy90Second.Quantity) {
        console.log('SELL FIRST.');
        return ('sell now');
      }
      */

      if (this.FixPriceSell === 0.0) {
        this.FixPriceSell = this.Stoploss;

        await this.setFixDatas();
      } else if (
        this.FixPriceSell < depthAsks40Plus1.Rate
          && depthAsks40Plus1.Rate < parseFloat(this.datasBids[bidsPosition].Rate)
          && this.RateBuyBuy < parseFloat(depthAsks40Plus21.Rate)
      ) {
        this.FixPriceSell = depthAsks40Plus1.Rate;
        // console.log(this.market, depthAsks40Plus1, depthAsks40Plus21);

        await this.setFixDatas();

        if (process.env.ROBOT_MODE === 'dev') {
          const trailng = {};
          trailng.Symbol = this.market;
          trailng.Fixprice = this.FixPriceSell;

          this.redissub.subscribe('buy', 'sell', 'trailing', () => {
            this.redispub.publish('trailing', JSON.stringify(trailng));
          });
        }
      } else {
        if (this.FixPriceSell >= parseFloat(this.datasBids[0].Rate)) {
          console.log('SELL SECOND.');
          return ('sell now');
        }

        return ('sell: nothing do');
      }
    } else return ('sell: nothing do');

    return ('sell: nothing do');
  }

  /*
  async main_old() {
    await this.setBuySellNow();
    const getBuyDatas = await this.getSellDatas();
    const depthAsks40Plus1 = await this.binancecalcs.calcForWall(40, 1);
    const depthAsks40Plus21 = await this.binancecalcs.calcForWall(40, 21);

    let bidsPosition = 0;
    if (process.env.ROBOT_MODE === 'dev') {
      bidsPosition = 21;
    }

    if (process.env.ROBOT_MODE === 'prod') {
      bidsPosition = 21;
    }

    if (getBuyDatas === null && bidsPosition !== 0) {
      if (this.FixPriceSell === 0.0) {
        this.FixPriceSell = this.Stoploss;

        await this.setFixDatas();
      } else if (
        this.FixPriceSell < depthAsks40Plus1.Rate
          && depthAsks40Plus1.Rate < parseFloat(this.datasBids[bidsPosition].Rate)
          && this.RateBuyBuy < parseFloat(depthAsks40Plus21.Rate)
      ) {
        this.FixPriceSell = depthAsks40Plus1.Rate;
        // console.log(this.market, depthAsks40Plus1, depthAsks40Plus21);

        await this.setFixDatas();

        if (process.env.ROBOT_MODE === 'dev') {
          const trailng = {};
          trailng.Symbol = this.market;
          trailng.Fixprice = this.FixPriceSell;

          this.redissub.subscribe('buy', 'sell', 'trailing', () => {
            this.redispub.publish('trailing', JSON.stringify(trailng));
          });
        }
      } else {
        if (this.FixPriceSell >= parseFloat(this.datasBids[0].Rate)) {
          return ('sell now');
        }

        return ('sell: nothing do');
      }
    } else return ('sell: nothing do');

    return ('sell: nothing do');
  }
  */

  getSellDatas() {
    return new Promise((resolve, reject) => {
      this.redis.pipeline([
        ['get', `${this.market}:thiswallpricesell`],
        ['get', `${this.market}:lastwallprice`],
        ['get', `${this.market}:amount`],
        ['get', `${this.market}:ratebuy`],
        ['get', `${this.market}:stoploss`],
        ['get', `${this.market}:raz`],
        ['get', `${this.market}:additionaldata`],
      ]).exec((err, results) => {
        if (err) reject(err);

        this.FixPriceSell = parseFloat(results[0][1]);
        this.LastPriceSell = parseFloat(results[1][1]);
        this.Amount = parseFloat(results[2][1]);
        this.RateBuyBuy = parseFloat(results[3][1]);
        this.Stoploss = parseFloat(results[4][1]);
        this.Raz = parseFloat(results[5][1]);
        this.stepsize = results[6][1];

        resolve(null);
      });
    });
  }

  setFixDatas() {
    return new Promise((resolve, reject) => {
      this.redis.pipeline([
        ['set', `${this.market}:thiswallpricesell`, this.FixPriceSell],
      ]).exec((err) => {
        if (err) reject(err);

        resolve(null);
      });
    });
  }

  setBuySellNow() {
    return new Promise((resolve, reject) => {
      this.redis.pipeline([
        ['set', `${this.market}:buysellnow`, this.datasBids[0].Rate],
      ]).exec((err) => {
        if (err) reject(err);

        resolve(null);
      });
    });
  }

  get FixPriceSell() {
    return parseFloat(this.fixPriceSell);
  }

  set FixPriceSell(value) {
    this.fixPriceSell = parseFloat(value);
  }

  get LastPriceSell() {
    return parseFloat(this.lastPriceSell);
  }

  set LastPriceSell(value) {
    this.lastPriceSell = parseFloat(value);
  }

  get Amount() {
    return parseFloat(this.amount);
  }

  set Amount(value) {
    this.amount = parseFloat(value);
  }

  get RateBuyBuy() {
    return parseFloat(this.rateBuyBuy);
  }

  set RateBuyBuy(value) {
    this.rateBuyBuy = parseFloat(value);
  }

  get Stoploss() {
    return parseFloat(this.stoploss);
  }

  set Stoploss(value) {
    this.stoploss = parseFloat(value);
  }

  get Raz() {
    return parseFloat(this.raz);
  }

  set Raz(value) {
    this.raz = parseFloat(value);
  }
}

module.exports = Algosell;