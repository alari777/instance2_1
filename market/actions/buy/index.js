// const settingsBuy = require('./setting-buy.json');

// Telegramm
const { TelegramClient } = require('messaging-api-telegram');

const clientTeleg = TelegramClient.connect(process.env.TELEGRAMM_CONNECT);
const uuidv4 = require('uuid/v4');
const Mysql = require('../../../mysql');
// const clientTeleg = TelegramClient.connect('645682996:AAFN9HTGK5od3_o4UTjcV6urI4NVSrPH-bk');

class Buy {
  constructor(connection, binancecalcs, algobuy, symbol) {
    this.redis = connection.redis;
    this.redissub = connection.redissub;
    this.redispub = connection.redispub;
    this.binance = connection.binance;
    // this.postgre = connection.postgre;
    // this.buysettings = settingsBuy;
    this.market = symbol;

    this.binancecalcs = binancecalcs;
    this.datasBids = binancecalcs.datasBids;
    this.datasAsks = binancecalcs.datasAsks;

    this.algobuy = algobuy;

    // this.postgreone = connection.connectToPostgre();

    return this;
  }

  /*
  async buyOrderDev() {
    const firstBids = parseFloat(this.datasBids[0].Rate);

    const range = firstBids - firstBids * 0.02;
    console.log(this.market, firstBids, range);
    console.log(this.datasBids);

    const objStoploss = await this.binancecalcs.calcWallInPercentRange(range);
    this.algobuy.stoploss = parseFloat(objStoploss.Rate);

    return new Promise((resolve, reject) => {
      console.log('Buy development. Start. Market is', this.market);
      const quantitySellTest = 0.0015 / this.algobuy.FixPriceBuy; // datasAsks[0].Rate
      const moncoinSellTest = `${this.market}&${quantitySellTest.toFixed(2).toString()}&40`;

      // const firstBids = parseFloat(this.datasBids[0].Rate);

      // this.algobuy.stoploss = firstBids - firstBids * 0.02;

      this.redis.pipeline([
        ['set', `${this.market}:buysellnow`, 0.0],
        ['set', `${this.market}:amount`, quantitySellTest.toFixed(2)],
        ['set', `${this.market}:typetrd`, 'sell'],
        ['set', `${this.market}:thiswallpricesell`, 0.0],
        ['set', `${this.market}:thiswallpricebuy`, 0.0],
        ['set', `${this.market}:thiswallvolumebuy`, 0.0],
        ['set', `${this.market}:lastwallprice`, 0.0],
        ['set', `${this.market}:ratebuy`, this.algobuy.FixPriceBuy],
        ['set', `${this.market}:stoploss`, this.algobuy.stoploss],
      ]).exec((err) => {
        if (err) {
          console.log(this.market, err);
          reject(err);
        }

        const part1 = parseFloat(this.datasAsks[0].Rate) - parseFloat(this.datasBids[0].Rate);
        const part2 = part1 / parseFloat(this.datasBids[0].Rate) * 100;
        const spread = part2.toFixed(2);

        const justsql = 'UPDATE public.monitoringsbinance_mar_total_sim SET amount = $1, typetrd=$2, moncoin=$3, note=$4, ratebuy=$5, stoploss=$6 WHERE (market = $7 AND container = $8 AND status = $9);';
        const queryArrU1 = [quantitySellTest.toFixed(2), 'sell', moncoinSellTest, spread, this.algobuy.FixPriceBuy, this.algobuy.stoploss, this.market, '1', true];
        this.postgre.query(justsql, queryArrU1, (errU1) => {
        // this.postgreone.query(justsql, queryArrU1, (errU1) => {
          if (errU1) console.log(this.market, errU1);
        });

        const dateInsertSim = new Date();
        console.log('Time:', dateInsertSim);
        const uuidv = uuidv4();

        const sqlN = 'INSERT INTO public.bsnew_sim_binance_mar_total_sim (uuidord, market, timeutcbuy, typeordbuy, quantitybuy, ratebuy, bodybuy, notebuy, statusbuy, timeutcsell, typeordsell, quantitysell, ratesell, bodysell, notesell, statussell, persent, active, note, status, stoploss) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14, $15, $16, $17, $18, $19, $20, $21);';
        const queryArrI2 = [uuidv, this.market, dateInsertSim, 'BUY', quantitySellTest.toFixed(2), this.algobuy.FixPriceBuy, '{}', '', 'OK', dateInsertSim, '', 0.0, 0.0, '{}', '', '', 0.0, true, '', '', this.algobuy.stoploss];
        this.postgre.query(sqlN, queryArrI2, (errInser2) => {
        // this.postgreone.query(sqlN, queryArrI2, (errInser2) => {
          if (errInser2) console.log(this.market, errInser2);

          if (!errInser2) {
            this.sendMessageViaTelegramm(0);

            const obj = {};
            obj.Symbol = this.market;
            obj.Action = 'BUY';
            obj.Buyprice = this.datasAsks[0].Rate;
            obj.Fixprice = this.algobuy.FixPriceBuy;
            obj.Raz = this.algobuy.Raz;
            obj.Sellprice = 0.0;
            obj.Stoploss = this.algobuy.stoploss;
            obj.Stepsize = this.algobuy.stepsize;

            this.redissub.subscribe('buy', 'sell', () => {
              this.redispub.publish('buy', JSON.stringify(obj));
            });
          } else {
            const obj = {};
            obj.code = '-1';
            obj.msg = 'Buy error: insert, bsnew.';
            this.sendBuyErrorViaTelegramm(JSON.stringify(obj));
          }
        });

        resolve('buy success');
      });
    });
  }
  */

  async buyOrderDevMysql() {
    const firstBids = parseFloat(this.datasBids[0].Rate);

    const range = firstBids - firstBids * 0.02;
    console.log(this.market, firstBids, range);
    console.log(this.datasBids);

    const objStoploss = await this.binancecalcs.calcWallInPercentRange(range);
    this.algobuy.stoploss = parseFloat(objStoploss.Rate);

    return new Promise((resolve, reject) => {
      console.log('Buy development. Start. Market is', this.market);
      const quantitySellTest = 0.0015 / this.algobuy.FixPriceBuy; // datasAsks[0].Rate
      const moncoinSellTest = `${this.market}&${quantitySellTest.toFixed(2).toString()}&40`;

      // const firstBids = parseFloat(this.datasBids[0].Rate);

      // this.algobuy.stoploss = firstBids - firstBids * 0.02;

      this.redis.pipeline([
        ['set', `${this.market}:buysellnow`, 0.0],
        ['set', `${this.market}:amount`, quantitySellTest.toFixed(2)],
        ['set', `${this.market}:typetrd`, 'sell'],
        ['set', `${this.market}:thiswallpricesell`, 0.0],
        ['set', `${this.market}:thiswallpricebuy`, 0.0],
        ['set', `${this.market}:thiswallvolumebuy`, 0.0],
        ['set', `${this.market}:lastwallprice`, 0.0],
        ['set', `${this.market}:ratebuy`, this.algobuy.FixPriceBuy],
        ['set', `${this.market}:stoploss`, this.algobuy.stoploss],
      ]).exec((err) => {
        if (err) {
          console.log(this.market, err);
          reject(err);
        }

        const part1 = parseFloat(this.datasAsks[0].Rate) - parseFloat(this.datasBids[0].Rate);
        const part2 = part1 / parseFloat(this.datasBids[0].Rate) * 100;
        const spread = part2.toFixed(2);

        const mysql = new Mysql();
        const justsql = 'UPDATE monitoringsbinance_mar_total_sim SET amount = ?, typetrd = ?, moncoin = ?, note = ?, ratebuy = ?, stoploss = ? WHERE (market = ? AND container = ? AND status = ?);';
        const queryArrU1 = [quantitySellTest.toFixed(2), 'sell', moncoinSellTest, spread, this.algobuy.FixPriceBuy, this.algobuy.stoploss, this.market, '1', true];
        mysql.connection.query(justsql, queryArrU1, (errU1) => {
          if (errU1) console.log(this.market, errU1);
        });

        const dateInsertSim = new Date();
        console.log('Time:', dateInsertSim);
        const uuidv = uuidv4();

        const params = {
          uuidord : uuidv,
          market : this.market,
          timeutcbuy : dateInsertSim,
          typeordbuy : 'BUY',
          quantitybuy : quantitySellTest.toFixed(2),
          ratebuy : this.algobuy.FixPriceBuy,
          bodybuy : '{}',
          notebuy : '',
          statusbuy : 'OK',
          timeutcsell : dateInsertSim,
          typeordsell : '',
          quantitysell : 0.0,
          ratesell : 0.0,
          bodysell : '{}',
          notesell : '',
          statussell : '',
          persent : 0.0,
          active : 1, // true
          note : '',
          status : '',
          stoploss : this.algobuy.stoploss
        };

        mysql.connection.query('INSERT INTO bsnew_sim_binance_mar_total_sim SET ?', params, (errInser2) => {
          if (errInser2) console.log(this.market, errInser2);

          if (!errInser2) {
            this.sendMessageViaTelegramm(0);

            const obj = {};
            obj.Symbol = this.market;
            obj.Action = 'BUY';
            obj.Buyprice = this.datasAsks[0].Rate;
            obj.Fixprice = this.algobuy.FixPriceBuy;
            obj.Raz = this.algobuy.Raz;
            obj.Sellprice = 0.0;
            obj.Stoploss = this.algobuy.stoploss;
            obj.Stepsize = this.algobuy.stepsize;

            this.redissub.subscribe('buy', 'sell', () => {
              this.redispub.publish('buy', JSON.stringify(obj));
            });
          } else {
            const obj = {};
            obj.code = '-1';
            obj.msg = 'Buy error: insert, bsnew.';
            this.sendBuyErrorViaTelegramm(JSON.stringify(obj));
          }
        });

        // mysql.doneMysql();
        mysql.connection.end((err) => {
          resolve('buy success');
        });
      });
    });
  }

  /*
  buyOrderProd() {
    return new Promise((resolve, reject) => {
      console.log('Buy production. Start. Market is', this.market);

      let quantitySellProd = 0.0015 / parseFloat(this.datasAsks[0].Rate);
      quantitySellProd = quantitySellProd.toFixed(this.algobuy.Raz);
      console.log('quantitySellProd', this.market, quantitySellProd, this.algobuy.Raz, this.datasAsks[0].Rate);

      if (parseFloat(quantitySellProd) !== 0.0) {
        const flags = { type: 'MARKET' };
        this.binance.robot.marketBuy(this.market, quantitySellProd, flags, (error, response) => {
          try {
            if (error) {
              console.log('Error production buy:', this.market, 'Error:', error.body, typeof error.body);
              this.sendBuyErrorViaTelegramm(error.body);
              this.redis.pipeline([
                // ['set', `${this.market}:typetrd`, 'buy'],
                ['set', `${this.market}:buysellnow`, 0.0],
                ['set', `${this.market}:thiswallpricesell`, 0.0],
                ['set', `${this.market}:thiswallpricebuy`, 0.0],
                ['set', `${this.market}:thiswallvolumebuy`, 0.0],
                ['set', `${this.market}:lastwallprice`, 0.0],
                ['set', `${this.market}:stoploss`, 0.0],
              ]).exec(() => {
              });

              this.algobuy.FixPriceBuy = 0.0;
              this.algobuy.FixVolumeBuy = 0.0;
              this.algobuy.stoploss = 0.0;

              reject(error.body);
            }

            if (!error) {
              console.log('Buy production. Status is success. Market:', this.market);
              console.log('Buy production. MarketBuy response is', response);
              let totalSum = 0.0;
              response.fills.forEach((dataset) => {
                totalSum = parseFloat(totalSum) + parseFloat(dataset.qty);
              });

              totalSum = totalSum.toFixed(this.algobuy.Raz);
              const firstBids = parseFloat(this.datasBids[0].Rate);
              this.algobuy.stoploss = firstBids - firstBids * 0.01;
              // this.stoploss = parseFloat(this.datasBids[19].Rate);

              this.redis.pipeline([
                ['set', `${this.market}:buysellnow`, 0.0],
                ['set', `${this.market}:amount`, totalSum.toString()], // response.fills[0].qty],
                ['set', `${this.market}:typetrd`, 'sell'],
                ['set', `${this.market}:thiswallpricesell`, 0.0],
                ['set', `${this.market}:thiswallpricebuy`, 0.0],
                ['set', `${this.market}:thiswallvolumebuy`, 0.0],
                ['set', `${this.market}:lastwallprice`, 0.0],
                ['set', `${this.market}:ratebuy`, response.fills[0].price],
                ['set', `${this.market}:stoploss`, this.algobuy.stoploss],
                // ['set', this.market + ':spread', spread.toString()]
              ]).exec((err) => {
                if (err) {
                  this.algobuy.FixPriceBuy = 0.0;
                  this.algobuy.FixVolumeBuy = 0.0;
                  reject(err);
                }

                const firstBids1 = parseFloat(this.datasBids[0].Rate);
                const firstAsks1 = parseFloat(this.datasAsks[0].Rate);
                const part1 = firstAsks1 - firstBids1;
                const part2 = part1 / parseFloat(this.datasBids[0].Rate) * 100;
                const spread = part2.toFixed(2);

                const moncoinProd = `${this.market}&${totalSum.toString()}&40`;
                const justsql = 'UPDATE public.monitoringsbinance SET amount = $1, typetrd=$2, moncoin=$3, note=$4, ratebuy=$5, stoploss=$6 WHERE (market = $7 AND container = $8 AND status = $9);';
                const queryArrU2 = [totalSum, 'sell', moncoinProd, spread, response.fills[0].price, this.algobuy.stoploss, this.market, '1', true];
                this.postgre.query(justsql, queryArrU2, (errU2) => {
                  if (errU2) console.log(this.market, errU2);
                });

                this.sendMessageViaTelegramm(response.fills[0].price);

                // (sell - buy) / buy * 100
                const dateInsertSim = new Date();
                console.log('Time:', dateInsertSim);
                const orderId = response.orderId; // uuidv4();
                console.log('order buy id():', orderId);

                const sqlNtotal = 'INSERT INTO public.bsnew_sim_binance_total (uuidord, market, timeutcbuy, typeordbuy, quantitybuy, ratebuy, bodybuy, notebuy, statusbuy, timeutcsell, typeordsell, quantitysell, ratesell, bodysell, notesell, statussell, persent, active, note, status, stoploss) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14, $15, $16, $17, $18, $19, $20, $21);';
                const queryArrI2 = [orderId, this.market, dateInsertSim, 'BUY', totalSum, response.fills[0].price, response, '', 'OK', dateInsertSim, '', 0.0, 0.0, '{}', '', '', 0.0, true, '', '', this.algobuy.stoploss];
                this.postgre.query(sqlNtotal, queryArrI2, (errI2) => {
                  if (errI2) console.log(this.market, errI2);
                });


                // const obj = {};
                // obj.Symbol = this.market;
                // obj.Action = 'BUY';
                // obj.Buyprice = this.datasAsks[0].Rate;
                // obj.Fixprice = this.algobuy.FixPriceBuy;
                // obj.Sellprice = 0.0;

                // this.redissub.subscribe('buy', 'sell', () => {
                //   this.redispub.publish('buy', JSON.stringify(obj));
                // });

                resolve('buy success');
              });
            }
          } catch (err) {
            console.log('Error buy:', this.market, 'Catch:', err);
            reject(err);
          }
        });
      } else {
        resolve('quantitySellProd is 0', this.market);
      }
    });
  }
  */

  freezeBuyMarket(freeze) {
    return new Promise((resolve, reject) => {
      this.redis.pipeline([
        ['set', `${this.market}:typetrd`, freeze],
      ]).exec((err) => {
        if (err) reject(err);

        resolve(null);
      });
    });
  }

  sendBuyErrorViaTelegramm(str) {
    let msg = '';

    const obj = JSON.parse(str);
    const codeError = obj.code;
    const msgError = obj.msg;

    msg = `🔔 Error in BUY. ${this.MarketShort}`;
    msg += `\n Code: ${codeError}`;
    msg += `\n Message: ${msgError}`;
    msg += `\n Inst: ${process.env.INST_NUMBER}`;


    clientTeleg.sendMessage(142545448, msg, {
      disable_web_page_preview: true,
      disable_notification: true,
    });
    clientTeleg.sendMessage(159856577, msg, {
      disable_web_page_preview: true,
      disable_notification: true,
    });
  }

  sendMessageViaTelegramm(price) {
    let msg = '';

    if (process.env.ROBOT_MODE === 'dev') {
      msg = `🔔 ${this.MarketShort}`;
      // msg += `\n📗 BUY PRICE: ${this.datasAsks[0].Rate}`;
      msg += `\n📗 BUY PRICE: ${this.algobuy.FixPriceBuy}`;
      msg += `\n⛔ STOPLOSS: ${this.algobuy.stoploss.toFixed(8)}`;

      clientTeleg.sendMessage(-1001298665688, msg, {
        disable_web_page_preview: true,
        disable_notification: true,
      });

      this.redis.pipeline([
        ['get', `${this.market}:trades`],
      ]).exec((err, results) => {
        if (results[0][1] !== null) {
          const foo = JSON.parse(results[0][1]);
          let str = '';
          foo.forEach((dataset) => {
            const dateNow0 = new Date();
            const dateNow1 = new Date(dataset.time);
            const h = dateNow1.getHours();
            const m = dateNow1.getMinutes();
            const s = dateNow1.getSeconds();
            const time = `${h}:${m}:${s}`;
            const diff1 = (parseFloat(dateNow0.getTime()) - parseFloat(dataset.time)) / 1000;
            if (diff1 <= 60.0) {
              let type = '';
              if (!dataset.isBuyerMaker) {
                type = 'buy';
              } else {
                type = 'sell';
              }
              str += `${dataset.price} ${dataset.qty} ${time} ${type}\n`;
            }
          });
          clientTeleg.sendMessage(142545448, str, {
            disable_web_page_preview: true,
            disable_notification: true,
          });
          clientTeleg.sendMessage(159856577, str, {
            disable_web_page_preview: true,
            disable_notification: true,
          });
        } else {
          console.log(results);
        }
      });
    }

    if (process.env.ROBOT_MODE === 'prod') {
      msg = `🔔 ${this.MarketShort}`;
      msg += `\n📗 BUY PRICE: ${price}`;
      msg += `\n⛔ STOPLOSS: ${this.algobuy.stoploss}`;

      clientTeleg.sendMessage(142545448, msg, {
        disable_web_page_preview: true,
        disable_notification: true,
      });
      clientTeleg.sendMessage(159856577, msg, {
        disable_web_page_preview: true,
        disable_notification: true,
      });
    }

    if (msg === '') {
      console.log('Buy. Something wrong on telegremm`s side. Message was not sended. Market is', this.MarketShort);
    }
  }

  get MarketShort() {
    return this.market.replace('BTC', '-BTC');
  }

  set MarketShort(value) {
    this.market = this.market.trim;
  }
}

module.exports = Buy;