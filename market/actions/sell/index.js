// const settingsSell  = require('./setting-sell.json');

// Telegramm
const { TelegramClient } = require('messaging-api-telegram');

const clientTeleg = TelegramClient.connect(process.env.TELEGRAMM_CONNECT);
const Mysql = require('../../../mysql');
// const uuidv4 = require('uuid/v4');

class Sell {
  constructor(connection, binancecalcs, algosell, symbol) {
    this.redis = connection.redis;
    this.redissub = connection.redissub;
    this.redispub = connection.redispub;
    this.binance = connection.binance;
    // this.postgre = connection.postgre;
    // this.sellsettings = settingsSell;
    this.market = symbol;

    this.binancecalcs = binancecalcs;
    this.datasBids = binancecalcs.datasBids;
    this.datasAsks = binancecalcs.datasAsks;

    this.algosell = algosell;

    // this.postgreone = connection.connectToPostgre();

    return this;
  }

  /*
  sellOrderDev() {
    return new Promise((resolve, reject) => {
      console.log('Sell (msg #1) TEST()', this.market);
      const quantityBuy = 0.0015;
      const moncoinBuy = `${this.market}&${quantityBuy.toString()}&40`;
      this.redis.pipeline([
        ['set', `${this.market}:buysellnow`, 0.0],
        ['set', `${this.market}:amount`, quantityBuy],
        ['set', `${this.market}:typetrd`, 'buy'],
        ['set', `${this.market}:thiswallpricesell`, 0.0],
        ['set', `${this.market}:thiswallpricebuy`, 0.0],
        ['set', `${this.market}:lastwallprice`, 0.0],
        ['set', `${this.market}:tradehistorybuy`, 0.0],
        ['set', `${this.market}:ratebuy`, 0.0],
        ['set', `${this.market}:stoploss`, 0.0],
      ]).exec((err) => {
        if (err) reject(err);

        const justsql = 'UPDATE public.monitoringsbinance_mar_total_sim SET amount = $1, typetrd=$2, moncoin=$3, ratesell=$4, stoploss=$5 WHERE (market = $6 AND container = $7 AND status = $8);';
        const arrUp1 = [0.0015, 'buy', moncoinBuy, this.datasBids[0].Rate, 0.0, this.market, '1', true];
        this.postgre.query(justsql, arrUp1, (errU1) => {
          // this.postgreone.query(justsql, arrUp1, (errU1) => {
          if (errU1) console.log(this.market, errU1);
        });

        const dateInsert = new Date();
        console.log('Time stoploss(msg #2) ', this.market, dateInsert);

        const dateInsertSim = new Date();
        const sqlI = 'SELECT * FROM public.bsnew_sim_binance_mar_total_sim WHERE market = $1 ORDER BY timeutcbuy DESC LIMIT 1';
        this.postgre.query(sqlI, [this.market], (errS1, result) => {
          // this.postgreone.query(sqlI, [this.market], (errS1, result) => {
          if (errS1) console.log(this.market, errS1);

          if (result.rowCount === 1) {
            result.rows.forEach((dataset1) => {
              const sqlU = 'UPDATE public.bsnew_sim_binance_mar_total_sim SET timeutcsell=$1, typeordsell=$2, quantitysell=$3, ratesell=$4, bodysell=$5, notesell=$6, statussell=$7 WHERE (uuidord = $8);';
              const arrUp2 = [dateInsertSim, 'SELL', this.algosell.Amount.toFixed(4), this.datasBids[0].Rate, '{}', '', 'OK', dataset1.uuidord];
              this.postgre.query(sqlU, arrUp2, (errUp1) => {
                // this.postgreone.query(sqlU, arrUp2, () => {
                if (!errUp1) {
                  this.sendMessageViaTelegramm(0);

                  const obj = {};
                  obj.Symbol = this.market;
                  obj.Action = 'SELL';
                  obj.Buyprice = 0.0;
                  obj.Fixprice = this.algosell.FixPriceSell;
                  obj.Raz = this.algosell.Raz;
                  obj.Sellprice = 0.0;
                  obj.Stepsize = this.algosell.stepsize;

                  this.redissub.subscribe('buy', 'sell', () => {
                    this.redispub.publish('sell', JSON.stringify(obj));
                  });
                } else {
                  const obj = {};
                  obj.code = '-2';
                  obj.msg = 'Sell error: update, bsnew.';
                  this.sendSellErrorViaTelegramm(JSON.stringify(obj));
                }
              });
            });
          }
        });

        resolve('sell success');
      });
    });
  }
  */

  sellOrderDevMysql() {
    return new Promise((resolve, reject) => {
      console.log('Sell (msg #1) TEST()', this.market);
      const quantityBuy = 0.0015;
      const moncoinBuy = `${this.market}&${quantityBuy.toString()}&40`;
      this.redis.pipeline([
        ['set', `${this.market}:buysellnow`, 0.0],
        ['set', `${this.market}:amount`, quantityBuy],
        ['set', `${this.market}:typetrd`, 'buy'],
        ['set', `${this.market}:thiswallpricesell`, 0.0],
        ['set', `${this.market}:thiswallpricebuy`, 0.0],
        ['set', `${this.market}:lastwallprice`, 0.0],
        ['set', `${this.market}:tradehistorybuy`, 0.0],
        ['set', `${this.market}:ratebuy`, 0.0],
        ['set', `${this.market}:stoploss`, 0.0],
      ]).exec((err) => {
        if (err) reject(err);

        const mysql = new Mysql();
        const justsql = 'UPDATE monitoringsbinance_mar_total_sim SET amount = ?, typetrd = ?, moncoin = ?, ratesell = ?, stoploss = ? WHERE (market = ? AND container = ? AND status = ?);';
        const arrUp1 = [0.0015, 'buy', moncoinBuy, this.datasBids[0].Rate, 0.0, this.market, '1', 1];
        mysql.connection.query(justsql, arrUp1, (errU1) => {
          if (errU1) console.log(this.market, errU1);
        });

        const dateInsert = new Date();
        console.log('Time stoploss(msg #2) ', this.market, dateInsert);

        const dateInsertSim = new Date();

        const sqlI = 'SELECT * FROM bsnew_sim_binance_mar_total_sim WHERE market = ? ORDER BY timeutcbuy DESC LIMIT 1';
        mysql.connection.query({
          sql: sqlI,
          timeout: 40000,
          values: [this.market],
        }, (errS1, result) => {
          if (errS1) console.log(this.market, errS1);

          if (result.length === 1) {
            result.forEach((dataset1) => {
              const sqlU = 'UPDATE bsnew_sim_binance_mar_total_sim SET timeutcsell = ?, typeordsell = ?, quantitysell = ?, ratesell = ?, bodysell = ?, notesell = ?, statussell = ? WHERE (uuidord = ?);';
              const arrUp2 = [dateInsertSim, 'SELL', this.algosell.Amount.toFixed(4), this.datasBids[0].Rate, '{}', '', 'OK', dataset1.uuidord];
              mysql.connection.query(sqlU, arrUp2, (errUp1) => {
                if (!errUp1) {
                  this.sendMessageViaTelegramm(0);

                  const obj = {};
                  obj.Symbol = this.market;
                  obj.Action = 'SELL';
                  obj.Buyprice = 0.0;
                  obj.Fixprice = this.algosell.FixPriceSell;
                  obj.Raz = this.algosell.Raz;
                  obj.Sellprice = 0.0;
                  obj.Stepsize = this.algosell.stepsize;

                  this.redissub.subscribe('buy', 'sell', () => {
                    this.redispub.publish('sell', JSON.stringify(obj));
                  });

                  // mysql.doneMysql();
                  mysql.connection.end((err) => {
                    resolve('sell success');
                  });
                } else {
                  const obj = {};
                  obj.code = '-2';
                  obj.msg = 'Sell error: update, bsnew.';
                  this.sendSellErrorViaTelegramm(JSON.stringify(obj));
                  // mysql.doneMysql();
                  mysql.connection.end((err) => {
                    resolve('sell success');
                  });
                }
              });
            });
          } else {
            // mysql.doneMysql();
            mysql.connection.end((err) => {
              resolve('sell success');
            });
          }
        });

        resolve('sell success');
      });
    });
  }

  /*
  sellOrderProd() {
    return new Promise((resolve, reject) => {
      console.log('Sell production. Start. Market is', this.market);
      const flags = { type: 'MARKET' };
      this.binance.robot.marketSell(this.market, this.algosell.Amount, flags, (error, response) => {
        try {
          if (error) {
            console.log('Error production sell:', this.market, 'Error:', error.body, typeof error.body);
            this.sendSellErrorViaTelegramm(error.body);
            reject(error);
          }

          if (!error) {
            console.log('Sell production. Status is success. Market:', this.market);
            console.log('Sell production. MarketSell response is', response);
            let totalSumSell = 0.0;
            response.fills.forEach((dataset) => {
              totalSumSell = parseFloat(totalSumSell) + parseFloat(dataset.qty);
            });

            totalSumSell = totalSumSell.toFixed(this.algosell.Raz);

            const quantityBuyProd = 0.0015;
            const moncoinBuyProd = `${this.market}&${quantityBuyProd.toString()}&40`;
            this.redis.pipeline([
              ['set', `${this.market}:buysellnow`, 0.0],
              ['set', `${this.market}:amount`, quantityBuyProd],
              ['set', `${this.market}:typetrd`, 'buy'],
              ['set', `${this.market}:thiswallpricesell`, 0.0],
              ['set', `${this.market}:thiswallpricebuy`, 0.0],
              ['set', `${this.market}:lastwallprice`, 0.0],
              ['set', `${this.market}:tradehistorybuy`, 0.0],
              ['set', `${this.market}:ratebuy`, 0.0],
              ['set', `${this.market}:ratesell`, 0.0],
              ['set', `${this.market}:stoploss`, 0.0],
            ]).exec((err) => {
              if (err) {
                console.log(err);
                reject(err);
              }

              const justsql = 'UPDATE public.monitoringsbinance SET amount = $1, typetrd=$2, moncoin=$3, ratesell=$4, stoploss=$5 WHERE (market = $6 AND container = $7 AND status = $8);';
              this.postgre.query(justsql, [0.0015, 'buy', moncoinBuyProd, response.fills[0].price, 0.0, this.market, '1', true], (errU1) => {
                if (errU1) console.log(this.market, errU1);
              });

              const dateInsert = new Date();
              console.log('Time stoploss(msg #2) ', this.market, dateInsert);

              this.sendMessageViaTelegramm(response.fills[0].price);

              const dateInsertSim = new Date();

              const sqlItotal = 'SELECT * FROM public.bsnew_sim_binance_total WHERE market = $1 ORDER BY timeutcbuy DESC LIMIT 1';
              this.postgre.query(sqlItotal, [this.market], (errS2, result) => {
                if (errS2) console.log(this.market, errS2);

                if (result.rowCount === 1) {
                  result.rows.forEach((dataset1) => {
                    const sqlUtotal = 'UPDATE public.bsnew_sim_binance_total SET timeutcsell=$1, typeordsell=$2, quantitysell=$3, ratesell=$4, bodysell=$5, notesell=$6, statussell=$7 WHERE (uuidord = $8);';
                    this.postgre.query(sqlUtotal, [dateInsertSim, 'SELL', totalSumSell, response.fills[0].price, response, '', 'OK', dataset1.uuidord], () => {
                    });
                  });
                } else {
                  console.log(this.market, '(): error in UPDATE public.bsnew_sim_binance_total');
                }
              });

              // const obj = {};
              // obj.Symbol = this.market;
              // obj.Action = 'sell';
              // obj.Buyprice = 0.0;
              // obj.Fixprice = response.fills[0].price;
              // obj.Sellprice = 0.0;

              // this.redissub.subscribe('buy', 'sell', () => {
              //   this.redispub.publish('sell', JSON.stringify(obj));
              // });

              resolve('sell success');
            });
          }
        } catch (err) {
          console.log('Error sell:', this.market, 'Catch:', err);
          reject(err);
        }
      });
    });
  }
  */

  freezeSellMarket(freeze) {
    return new Promise((resolve, reject) => {
      this.redis.pipeline([
        ['set', `${this.market}:typetrd`, freeze],
      ]).exec((err) => {
        if (err) reject(err);

        resolve(null);
      });
    });
  }

  sendSellErrorViaTelegramm(str) {
    let msg = '';

    const obj = JSON.parse(str);
    const codeError = obj.code;
    const msgError = obj.msg;

    msg = `🔔 Error in SELL. ${this.MarketShort}`;
    msg += `\n Code: ${codeError}`;
    msg += `\n Message: ${msgError}`;
    msg += `\n Inst: ${process.env.INST_NUMBER}`;

    clientTeleg.sendMessage(142545448, msg, {
      disable_web_page_preview: true,
      disable_notification: true,
    });
    clientTeleg.sendMessage(159856577, msg, {
      disable_web_page_preview: true,
      disable_notification: true,
    });
  }

  sendMessageViaTelegramm(price) {
    let msg = '';

    if (process.env.ROBOT_MODE === 'dev') {
      msg = `🔔 ${this.MarketShort}`;
      msg += `\n📕 SELL PRICE: ${this.datasBids[0].Rate}`;

      clientTeleg.sendMessage(-1001298665688, msg, {
        disable_web_page_preview: true,
        disable_notification: true,
      });
    }

    if (process.env.ROBOT_MODE === 'prod') {
      msg = `🔔 ${this.MarketShort}`;
      msg += `\n📕 SELL PRICE: ${price}`;

      clientTeleg.sendMessage(142545448, msg, {
        disable_web_page_preview: true,
        disable_notification: true,
      });
      clientTeleg.sendMessage(159856577, msg, {
        disable_web_page_preview: true,
        disable_notification: true,
      });
    }

    if (msg === '') {
      console.log('Sell. Something wrong on telegremm`s side. Message was not sended. Market is', this.MarketShort);
    }
  }

  get MarketShort() {
    return this.market.replace('BTC', '-BTC');
  }

  set MarketShort(value) {
    this.market = this.market.trim;
  }
}

module.exports = Sell;