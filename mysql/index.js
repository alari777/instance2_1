require('dotenv').config();
const mysql = require('mysql');

class Mysql {
    constructor() {
        this.connection = mysql.createConnection({
            host: process.env.MYSQL_HOST,
            user: process.env.MYSQL_USER,
            password: process.env.MYSQL_PASSWORD,
            database: process.env.MYSQL_DB
        });

        return this.connection.connect();
    }

    async doneMysql() {
        return new Promise((resolve) => {
            this.connection.end((err) => {
                resolve(null);
            });
        });
    }
}

module.exports = Mysql;