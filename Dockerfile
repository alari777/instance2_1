FROM node:carbon

# Install software 
RUN apt-get install -y git
RUN npm install forever -g
VOLUME /binance

RUN git clone https://alari777@bitbucket.org/alari777/instance2_1.git /binance/

# Create app directory
WORKDIR /binance

EXPOSE 80
CMD git pull origin master && npm install -y && npm start