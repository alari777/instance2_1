class Binancekeeping {
  constructor(connection, settings) {
    this.redis = connection.redis;
    // this.redispub = connection.redispub;
    this.redissubkeep = connection.redissubkeep;
    this.redispubkeep = connection.redispubkeep;

    // this.postgre = connection.postgre;
    this.binance = connection.binance;

    this.depthCacheBinance = settings.depthCacheBinance;
    this.lenBids = parseFloat(process.env.SETTING_LEN_BIDS); // settings.lenBids;
    this.lenAsks = parseFloat(process.env.SETTING_LEN_ASKS); // settings.lenAsks;

    this.bids = {};
    this.asks = {};

    // this.postgreone = connection.connectToPostgre();

    return this;
  }

  getBinanceOrders(markets) {
    let counter = 0;
    this.binance.robot.websockets.depthCache(markets, (symbol, depth) => {
      const bids = this.binance.robot.sortBids(depth.bids);
      const asks = this.binance.robot.sortAsks(depth.asks);

      const lenBids = Object.keys(bids).length;
      const lenAsks = Object.keys(asks).length;

      // let firstBids = this.binance.robot.first(bids);
      // let firstAsks = this.binance.robot.first(asks);

      if (lenBids >= this.lenBids && lenAsks >= this.lenAsks) {
        const objbids = {};
        const objasks = {};

        Object.keys(bids).forEach((key, index) => {
          if (index === 0 && symbol === 'IOTABTC') {
            // console.log('bids:', symbol, bids[key], key);
          }
          if (index <= 99) objbids[key] = bids[key];
        });

        // if (symbol === 'BNBBTC') {
        //   console.log('len bids', Object.keys(bids).length);
        //   console.log('len objbids', Object.keys(objbids).length);
        //   console.log(' ----------------- ');
        //   console.log(bids);
        //   console.log(objbids);
        // }

        Object.keys(asks).forEach((key, index) => {
          if (index === 0 && symbol === 'IOTABTC') {
            // console.log('asks:', symbol, asks[key], key);
          }
          if (index <= 99) objasks[key] = asks[key];
        });

        this.redis.pipeline([
          // ['set', `${symbol}:bids`, JSON.stringify(bids)],
          // ['set', `${symbol}:asks`, JSON.stringify(asks)],
          ['set', `${symbol}:bids`, JSON.stringify(objbids)],
          ['set', `${symbol}:asks`, JSON.stringify(objasks)],
        ]).exec(() => {
          counter += 1;

          this.redissubkeep.subscribe('depthcache', () => {
            this.redispubkeep.publish('depthcache', symbol);
          });

          this.redissubkeep.subscribe('depthcachesymbol', () => {
            const arr = [];
            // const newbids = bids;
            // const objbids = {};
            // const newasks = asks;
            // const objasks = {};

            // Object.keys(newbids).forEach((key, index) => {
            //   if (index <= 99) objbids[key] = newbids[key];
            // });

            // Object.keys(newasks).forEach((key, index) => {
            //   if (index <= 99) objasks[key] = newasks[key];
            // });
            arr.push(symbol);
            arr.push(objbids);
            arr.push(objasks);
            this.redispubkeep.publish('depthcachesymbol', JSON.stringify(arr));
          });

          /*
          if (counter === 5000) {
            console.log('Postgre check generall connect. Counter Ticks:', counter);
            counter = 0;
            const sql = 'SELECT * FROM public.monitoringsbinance_mar_total_sim LIMIT 1;';
            // this.postgre.query(sql, () => {});
            this.postgreone.query(sql, (err) => {
              if (err) console.log(err);
            });
          }
           */
        });
      }
    }, this.depthCacheBinance);
  }

  async recentTrades(symbol) {
    return new Promise((resolve, reject) => {
      const map = new Map();
      this.binance.history.recentTrades(symbol, (error, trades) => {
        if (error) reject(error);

        // console.log(symbol, trades.length);

        if (trades.length !== 0 && Array.isArray(trades)) {
          const dateNow1 = new Date();
          this.redis.pipeline([
            ['set', `${symbol}:trades`, JSON.stringify(trades)],
          ]).exec(() => { });

          trades.forEach((dataset) => {
            const diff1 = (parseFloat(dateNow1.getTime()) - parseFloat(dataset.time)) / 1000;
            if (diff1 <= 60.0 && !dataset.isBuyerMaker) {
              // console.log(symbol, diff1, dataset.price, dataset.qty, dataset.isBuyerMaker);
              let qty = 0.0;
              const price = parseFloat(dataset.price);
              if (map.has(price)) {
                qty = map.get(price);
                map.set(price, qty + parseFloat(dataset.qty));
              } else {
                map.set(price, parseFloat(dataset.qty));
              }
            } else {
              // let mapMarket = new Map();
              // mapMarket.set(symbol, map);
              resolve(map);
            }
          });
        } else {
          const errorTradesLen = {
            status: 'error trades length',
            market: symbol,
          };

          reject(errorTradesLen);
        }
      }, 100);
    });
  }
}

module.exports = Binancekeeping;