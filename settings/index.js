const settingsGenerall  = require('./generall/setting-generall.json');

class Settings {
	constructor() {
		this.generall = settingsGenerall;
		
		return this;
	}
}

module.exports = Settings;