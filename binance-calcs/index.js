class Binancecalcs {
  constructor(connection, settings, symbol) {
    this.redis = connection.redis;
    // this.redispub = connection.redispub;

    this.lenBids = settings.lenBids;
    this.lenAsks = settings.lenAsks;

    this.market = symbol;
    this.datasBids = [];
    this.datasAsks = [];
    return this;
  }

  makeWorkingBidsAndAsksObjs() {
    return new Promise((resolve, reject) => {
      this.redis.pipeline([
        ['get', `${this.market}:bids`],
        ['get', `${this.market}:asks`],
      ]).exec((err, results) => {
        if (err) reject(err);

        if (results[0][1] !== null && results[1][1] !== null) {
          const bids = JSON.parse(results[0][1]);
          const asks = JSON.parse(results[1][1]);

          Object.keys(bids).forEach((key) => {
            const obj = {};
            obj.Quantity = bids[key];
            obj.Rate = key;
            this.datasBids.push(obj);
          });

          Object.keys(asks).forEach((key) => {
            const obj = {};
            obj.Quantity = asks[key];
            obj.Rate = key;
            this.datasAsks.push(obj);
          });

          // console.log(this.market, datasBids[0].Rate, datasAsks[0].Rate);
          // console.log(this.marketName, Object.keys(bids).length, Object.keys(asks).length);
          // console.log(this.market, typeof results[0][1], typeof results[1][1]);
          const lenB = Object.keys(bids).length;
          const lenA = Object.keys(asks).length;
          if (lenB === this.datasBids.length && lenA === this.datasAsks.length) {
            // return "Done. MAke obj.";
            resolve(true);
          }
        } else {
          const error = {
            status: 'error: makeWorkingBidsAndAsksObjs',
            market: this.market,
          };

          reject(error);
        }
      });
    });
  }

  calcWall(depth) {
    return new Promise((resolve, reject) => {
      let asksForWall = {};
      if (this.datasAsks.length >= this.lenAsks) {
        // ASKS. Вырезаем нужной длины массив.
        let dynamicArrAsks = [];
        dynamicArrAsks = this.datasAsks.slice(0, depth);

        // ASKS. Сортируем массив.
        dynamicArrAsks.sort((obj1, obj2) => obj1.Quantity - obj2.Quantity);

        // ASKS. Стенка -- самый последний элемент.
        let asksMaxWall = {};
        asksMaxWall = dynamicArrAsks[dynamicArrAsks.length - 1];

        // ASKS. Стенка -- в исходном массиве. Это надо, для того,
        // чтобы можно было искать застенки или предстенки.
        // Также чтобы можно было искать ордера, стоящие
        // на N позиций ниже или выше стенки в настощем
        // стакане this.datasAsks.indexOf(asksMaxWall) -- это исходная позиция стенки
        // в исходном стакане
        asksForWall = this.datasAsks[this.datasAsks.indexOf(asksMaxWall)];
        if (!Binancecalcs.isEmptyObj(asksForWall)) {
          const obj = {};
          obj.Quantity = parseFloat(asksForWall.Quantity);
          obj.Rate = parseFloat(asksForWall.Rate);
          resolve(obj);
        }
      } else {
        const error = {
          status: 'error: calcWall',
          market: this.market,
          lenDataAsks: this.datasAsks.length,
          lenAsks: this.lenAsks,
        };
        reject(error);
      }
    });
  }

  calcWallSecond(depth) {
    return new Promise((resolve, reject) => {
      let asksForWall = {};
      if (this.datasAsks.length >= this.lenAsks) {
        // ASKS. Вырезаем нужной длины массив.
        let dynamicArrAsks = [];
        dynamicArrAsks = this.datasAsks.slice(0, depth);

        // ASKS. Сортируем массив.
        dynamicArrAsks.sort((obj1, obj2) => obj1.Quantity - obj2.Quantity);

        // ASKS. Стенка -- самый последний элемент.
        let asksMaxWall = {};
        asksMaxWall = dynamicArrAsks[dynamicArrAsks.length - 2];

        // ASKS. Стенка -- в исходном массиве. Это надо, для того,
        // чтобы можно было искать застенки или предстенки.
        // Также чтобы можно было искать ордера, стоящие
        // на N позиций ниже или выше стенки в настощем
        // стакане this.datasAsks.indexOf(asksMaxWall) -- это исходная позиция стенки
        // в исходном стакане
        asksForWall = this.datasAsks[this.datasAsks.indexOf(asksMaxWall)];
        if (!Binancecalcs.isEmptyObj(asksForWall)) {
          const obj = {};
          obj.Quantity = parseFloat(asksForWall.Quantity);
          obj.Rate = parseFloat(asksForWall.Rate);
          resolve(obj);
        }
      } else {
        const error = {
          status: 'error: calcWall',
          market: this.market,
          lenDataAsks: this.datasAsks.length,
          lenAsks: this.lenAsks,
        };
        reject(error);
      }
    });
  }

  calcWallInPercentRange(range) {
    return new Promise((resolve, reject) => {
      let bidsForWall = {};
      if (this.datasBids.length >= this.lenBids) {
        // ASKS. Вырезаем нужной длины массив.
        let dynamicArrBids = [];
        dynamicArrBids = this.datasBids.filter(obj => obj.Rate >= parseFloat(range));
        // dynamicArrBids = this.datasBids.slice(0, depth);

        // ASKS. Сортируем массив.
        dynamicArrBids.sort((obj1, obj2) => obj1.Quantity - obj2.Quantity);

        // ASKS. Стенка -- самый последний элемент.
        let bidsMaxWall = {};
        bidsMaxWall = dynamicArrBids[dynamicArrBids.length - 1];

        // ASKS. Стенка -- в исходном массиве. Это надо, для того,
        // чтобы можно было искать застенки или предстенки.
        // Также чтобы можно было искать ордера, стоящие
        // на N позиций ниже или выше стенки в настощем
        // стакане this.datasBids.indexOf(bidsMaxWall) -- это исходная позиция стенки
        // в исходном стакане
        if (bidsMaxWall >= 99) {
          bidsMaxWall = 99;
        }
        bidsForWall = this.datasBids[this.datasBids.indexOf(bidsMaxWall) + 1];
        if (!Binancecalcs.isEmptyObj(bidsForWall)) {
          const obj = {};
          obj.Quantity = parseFloat(bidsForWall.Quantity);
          obj.Rate = parseFloat(bidsForWall.Rate);
          resolve(obj);
          // resolve(bidsForWall);
        }
      } else {
        const error = {
          status: 'error: calcForWall',
          market: this.market,
          lenDataBids: this.datasBids.length,
          lenBids: this.lenBids,
        };
        reject(error);
      }
    });
  }

  calcForWall(depth, prewallnum) {
    return new Promise((resolve, reject) => {
      let bidsForWall = {};
      if (this.datasBids.length >= this.lenBids) {
        // ASKS. Вырезаем нужной длины массив.
        let dynamicArrBids = [];
        dynamicArrBids = this.datasBids.slice(0, depth);

        // ASKS. Сортируем массив.
        dynamicArrBids.sort((obj1, obj2) => obj1.Quantity - obj2.Quantity);

        // ASKS. Стенка -- самый последний элемент.
        let bidsMaxWall = {};
        bidsMaxWall = dynamicArrBids[dynamicArrBids.length - 1];

        // ASKS. Стенка -- в исходном массиве. Это надо, для того,
        // чтобы можно было искать застенки или предстенки.
        // Также чтобы можно было искать ордера, стоящие
        // на N позиций ниже или выше стенки в настощем
        // стакане this.datasBids.indexOf(bidsMaxWall) -- это исходная позиция стенки
        // в исходном стакане
        bidsForWall = this.datasBids[this.datasBids.indexOf(bidsMaxWall) + prewallnum];
        if (!Binancecalcs.isEmptyObj(bidsForWall)) {
          const obj = {};
          obj.Quantity = parseFloat(bidsForWall.Quantity);
          obj.Rate = parseFloat(bidsForWall.Rate);
          resolve(obj);
          // resolve(bidsForWall);
        }
      } else {
        const error = {
          status: 'error: calcForWall',
          market: this.market,
          lenDataBids: this.datasBids.length,
          lenBids: this.lenBids,
        };
        reject(error);
      }
    });
  }

  async calcWallsAndPreWalls(action) {
    let results = [];
    switch (action) {
      case 'getWalls':
        results = await Promise.all([
          this.calcWall(10),
          this.calcWall(20),
          this.calcWall(30),
        ]);
        break;

      case 'getForWalls':
        results = await Promise.all([
          this.calcForWall(10, 1),
          this.calcForWall(20, 5),
          this.calcForWall(30, 1),
        ]);
        break;

      default:
        break;
    }

    const obj = {
      market: this.market,
      action,
      orders: results,
    };

    return obj;
  }

  getFirstBids() {
    return this.datasBids[0];
  }

  getFirstAsks() {
    return this.datasAsks[0];
  }

  set marketName(symbol) {
    this.market = symbol;
  }

  get marketName() {
    return this.market;
  }

  static isEmptyObj(foo) {
    let result = true;
    Object.keys(foo).forEach((key) => {
      if (Object.prototype.hasOwnProperty.call(foo, key)) {
        result = false;
      }
    });
    return result;
  }
}

module.exports = Binancecalcs;