const Dbredis = require('./redis');

const redis = new Dbredis();

const Dbredissub = require('./redis');

const redissub = new Dbredissub();

const Dbredispub = require('./redis');

const redispub = new Dbredispub();

//
const Dbredissubkeep = require('./redis');

const redissubkeep = new Dbredissubkeep();

const Dbredispubkeep = require('./redis');

const redispubkeep = new Dbredispubkeep();
//

/*
const Dbpostgre = require('./postgre');

const postgre = new Dbpostgre();
postgre.connect();
*/

const ClientBinance = require('./binance');

const binance = new ClientBinance();

class Connection {
  constructor() {
    this.postgre = 5432;

    this.redis = redis;
    this.redissub = redissub;
    this.redispub = redispub;
    this.redissubkeep = redissubkeep;
    this.redispubkeep = redispubkeep;
    // this.postgre = postgre;
    this.binance = binance;

    return this;
  }

  /*
  connectToPostgre() {
    const postgreOne = new Dbpostgre({
      port: this.postgre,
      host: process.env.POSTGRE_HOST,
      user: process.env.POSTGRE_USER,
      password: process.env.POSTGRE_PASSWORD,
      database: process.env.POSTGRE_DATABASE,
    });
    postgreOne.connect();
    return postgreOne;
  }
  */
}

module.exports = Connection;