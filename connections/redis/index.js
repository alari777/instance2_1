const Redis = require('ioredis');
const settings = require('./settings-redis');

class Dbredis {
  constructor() {
    this.connection = Dbredis.connectToDb();

    return this.connection;
  }

  static connectToDb() {
    return new Redis({
      port: settings.port,
      host: process.env.REDIS_HOST,
      password: process.env.REDIS_PASSWORD,
      db: process.env.REDIS_DB,
    });
  }
}

module.exports = Dbredis;