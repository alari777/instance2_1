const pg = require('pg');
const settings = require('./settings-postgre');

class Dbpostgre {
  constructor() {
    this.connection = Dbpostgre.connectToDb();

    this.connection.on('error', (err) => {
      console.error('An idle client has experienced an error', err.stack);
      // New connect
      this.connection.connect();
    });

    return this.connection;
  }

  static connectToDb() {
    return new pg.Client({
      port: settings.port,
      host: process.env.POSTGRE_HOST,
      user: process.env.POSTGRE_USER,
      password: process.env.POSTGRE_PASSWORD,
      database: process.env.POSTGRE_DATABASE,
    });
  }
}

module.exports = Dbpostgre;