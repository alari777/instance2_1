const { TelegramClient } = require('messaging-api-telegram');

const clientTeleg = TelegramClient.connect(process.env.TELEGRAMM_CONNECT);
const settings = require('./settings');
// const Binance  = require('node-binance-api');

class Users {
  constructor(connection) {
    this.redissub = connection.redissub;
    this.postgre = connection.postgre;
    this.settings = settings;

    return this;
  }

  main() {
    this.redissub.on('message', (channel, message) => {
      // let obj = JSON.parse(message);
      // let symbol    = obj.Symbol;
      // let action    = obj.Action;
      // let buyprice  = obj.Buyprice;
      // let sellprice = obj.Sellprice;
      const amount = this.settings.amountUser1;
      console.log('Generall datas: %s, amount: %s. Channel: %s', message, amount, channel);
      // Users.sendMessageViaTelegramm(message, amount, channel);

      if (channel === 'trailing') {
        Users.sendTrailingViaTelegramm(message);
      }
    });
  }

  getUserDatas() {
    const sql = 'SELECT * FROM public.othersusers ORDER BY id ASC;';
    this.postgre.query(sql, (err, result) => {
      if (err) {
        console.log(this.market, err);
        return null;
      }

      if (result.rowCount >= 1) {
        result.rows.forEach((dataset) => {
          const map = new Map();
          map.set(dataset.apikey, dataset.secretkey);
        });
      } else {
        return null;
      }
      return null;
    });
  }

  static sendMessageViaTelegramm(message, amount, channel) {
    let msg = `New Release 3.0.1. Other Users.\n🔔 ${message}`;
    msg += `\n📕 AMOUNT: ${amount}`;
    msg += `\n📕 channel: ${channel}`;
    clientTeleg.sendMessage(142545448, msg, {
      disable_web_page_preview: true,
      disable_notification: true,
    });
    clientTeleg.sendMessage(159856577, msg, {
      disable_web_page_preview: true,
      disable_notification: true,
    });
  }

  static sendTrailingViaTelegramm(message) {
    const trailing = JSON.parse(message);
    const symbol = trailing.Symbol.replace('BTC', '-BTC');
    const stoploss = trailing.Fixprice;

    let msg = `🔔 ${symbol}`;
    msg += `\n💸 MOVE STOP-LOSS TO: ${stoploss}`;
    // let msg = '🔔 Trailing stop-loss';
    // msg += `\n📕 MARKET: ${symbol}`;
    // msg += `\n🚧 NEW STOPLOSS: ${stoploss}`;

    // clientTeleg.sendMessage(142545448, msg, {
    //   disable_web_page_preview: true,
    //   disable_notification: true,
    // });
    // clientTeleg.sendMessage(159856577, msg, {
    //   disable_web_page_preview: true,
    //   disable_notification: true,
    // });
    clientTeleg.sendMessage(-1001298665688, msg, {
      disable_web_page_preview: true,
      disable_notification: true,
    });
  }
}

module.exports = Users;