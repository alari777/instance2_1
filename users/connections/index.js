const Binance = require('node-binance-api');
const settings = require('./settings-binance');

class ClientBinanceOtherUser {
  constructor(connection, apikey, secretkey) {
    this.postgre = connection.postgre;
    this.apikey = apikey;
    this.secretkey = secretkey;

    this.connections = this.connectToDb();

    return this.connections;
  }

  connectToDb() {
    return new Binance().options({
      APIKEY: this.apikey,
      APISECRET: this.secretkey,
      reconnect: settings.allusers.reconnect,
      recvWindow: settings.allusers.recvWindow,
      useServerTime: settings.allusers.useServerTime,
      verbose: settings.allusers.verbose,
      test: settings.allusers.test,
    });
  }
}

module.exports = ClientBinanceOtherUser;
