require('dotenv').config();
const ip = require('ip');

console.log('IP', ip.address());

const Connections = require('./connections');

const connections = new Connections();
// connections.num = 0.1;

const Preparation = require('./preparation');

// const preparation = new Preparation(connections.redis, connections.postgre);
const preparation = new Preparation(connections.redis);

const Settings = require('./settings');

const settings = new Settings();

const Binancekeeping = require('./binance-keeping');

const binancekeeping = new Binancekeeping(connections, settings.generall);

const Binancecalcs = require('./binance-calcs');

const Market = require('./market');
const Algorithmsbuy = require('./market/algorithms/buy');
const Actionsbuy = require('./market/actions/buy');
const Algorithmssell = require('./market/algorithms/sell');
const Actionssell = require('./market/actions/sell');

const Users = require('./users');

const users = new Users(connections);
users.main();

// const { performance } = require('perf_hooks');
// const log = require('./log')(module);

let markets = [];


function calculatingBinanceDatas() {
  // setInterval(() => {
  // const t0 = performance.now();

  connections.redissubkeep.on('message', async (channel, message) => {
    const dataset = message;
    if (markets.includes(dataset) && channel === 'depthcache') {
      // console.log(dataset, channel);
      // markets.forEach(async (dataset) => {
      try {
        const binancecalcs = new Binancecalcs(connections, settings.generall, dataset);
        await binancecalcs.makeWorkingBidsAndAsksObjs();

        const market = new Market(connections, binancecalcs, dataset);
        // Роутер на определение монеты: на покупке или на продаже она находится
        const typetrd = await market.router();
        // log.verbose('info', 'Market: %s, type: %s', dataset, typetrd);

        switch (typetrd) {
          case 'buy': {
            const algobuy = new Algorithmsbuy(connections, binancecalcs, binancekeeping, dataset);
            const algoresultbuy = await algobuy.main();
            if (algoresultbuy === 'buy now') {
              console.log(dataset, algoresultbuy);
              // console.log(dataset, marketHistory);

              const actionbuy = new Actionsbuy(connections, binancecalcs, algobuy, dataset);
              let buyactionresult = '';

              if (process.env.ROBOT_MODE === 'dev') {
                await actionbuy.freezeBuyMarket();
                // buyactionresult = await actionbuy.buyOrderDev();
                buyactionresult = await actionbuy.buyOrderDevMysql();
              }

              if (process.env.ROBOT_MODE === 'prod') {
                // await actionbuy.freezeBuyMarket('freeze');
                // buyactionresult = await actionbuy.buyOrderProd();
              }

              if (buyactionresult === 'buy success') {
                console.log(dataset, buyactionresult);
              }
            }
          }
            break;

          case 'sell': {
            const algosell = new Algorithmssell(connections, binancecalcs, dataset);
            const algoresultsell = await algosell.main();
            if (algoresultsell === 'sell now') {
              const actionsell = new Actionssell(connections, binancecalcs, algosell, dataset);
              let sellactionresult = '';
              if (process.env.ROBOT_MODE === 'dev') {
                await actionsell.freezeSellMarket('freeze');
                // sellactionresult = await actionsell.sellOrderDev();
                sellactionresult = await actionsell.sellOrderDevMysql();
              }
              if (process.env.ROBOT_MODE === 'prod') {
                // await actionsell.freezeSellMarket('freeze');
                // sellactionresult = await actionsell.sellOrderProd();
              }

              if (sellactionresult === 'sell success') {
                console.log(dataset, sellactionresult);
              }
            }
          }
            break;

          case 'freeze':
            // console.log(dataset, typetrd);
            break;

          default:
            break;
        }

        // console.log(dataset, typetrd);
      } catch (err) {
        // console.log(err);
      }
    // });
    }
  });

  // const t1 = performance.now();
  // console.log('Took', ( (t1 - t0) / 1000).toFixed(4), 'seconds');
  // }, 1000);
}


(async function robot() {
  // Подготовка данных. Получаем из PostGre монеты
  // Запихиваем актуальные данные по каждой монете в Redis
  try {
    const result = await preparation.getMarketsForWork();
    if (result === null) {
      console.log('No aviable markets for works');
      return false;
    }

    markets = result.step1.n;
    console.log('1.) Starting #1. Amount of markets:', markets.length);
    console.log('2.) Starting #2.', result.step2);

    // Запускаем Binance WebScokets для готовых к работе монет: ордера, маркет хистори
    binancekeeping.getBinanceOrders(markets);
  } catch (err) {
    // console.log(err);
  }


  // Запускаем обработку данных и алгоритмы: находим стенки, застенки,
  // определяем какой статус у монеты, алгоритм проверки покупки или
  // продажи, принятие решение о покупке или продаже и т.п.
  calculatingBinanceDatas();

  return true;
}());


// connections.redis.subscribe("depthCache", function (err, count) {
// connections.redispub.publish('depthCache', "Hello world!");
// });

// connections.redis.on('message', function (channel, message) {
// console.log('Market %s. Channel: %s', message, channel);
// });

// robot();
